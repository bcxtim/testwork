<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.01.16
 * Time: 19:56
 */

return [

    //auth form
    'USER_LOGIN' => "Login",
    'USER_PASSWORD' => "Password",
    'USER_REPASSWORD' => 'Confirm password',
    'AUTH_INCORRECT' => 'Incorrect login or password',
    'USER_ENTER' => 'Login',
    'NO_AVATAR' => 'No avatar',

    //header
    'TITLE' => 'Test site',
    'LINK_LOGIN' => 'Login',
    'LINK_REG' => 'Registration',
    'LINK_LOGOUT' => 'logout',
    'OR' => 'or',
    'LOGIN' => 'Login',

    //footer
    'FOOTER' => 'All rights reserved',

    //registration form
    'REG_TITLE' => 'Registration',
    'REG_MESSAGE' => 'Please, complete all fields',
    'USER_ID' => 'id',
    'USER_FIRST_NAME' => 'First name',
    'USER_LAST_NAME' => 'Last name',
    'USER_EMAIL' => 'E-mail',
    'USER_SEX' => 'Sex',
    'USER_SEX_MALE' => 'Male',
    'USER_SEX_FEMALE' => 'Female',
    'USER_BIRTH_DATE' => 'Birth date',
    'USER_AVATAR' => 'Avatar',
    'USER_SIGN_UP' => 'Sign up',
    'USER_DATE_REG' => 'Date register',

    //index
    'HELLO' => 'Hello',
    'USER_DATA' => 'Your Profile:',
    'TEXT_INDEX' => 'For information about yourself, you need to register.',

    //ошибки обработки формы
    'LOGIN_LENGTH' => 'The length of the login can not be less than 3 characters, and more than 10 characters',
    'PASSWORD_LENGTH' => 'The password can not be less than 6 characters and more than 12 characters',
    'LOGIN_LAT' => 'Login may only consist of Latin letters',
    'INCORECT_EMAIL' => 'Please enter a valid e-mail',
    'LOGIN_USE' => 'This login already exists',
    'EMAIL_USE' => 'This e-mail already exists',
    'FILE_TYPE' => 'Файл может быть форматов: .gif, jpg, png"',
    'FILE_SIZE' => 'File size must not exceed 1 MB',
    'ERROR_MESSAGE' => 'When registering emerged following error:',
    'REPASS_INCORRECT' => 'Passwords do not match',
    'LOGIN_EMPTY' => 'Please, enter your login',
    'FIRSTNAME_EMPTY' => 'Please, enter your firstname',
    'LASTNAME_EMPTY' => 'Please, enter your lastname',
    'PASS_EMPTY' => 'Please, enter your password',
    'REPASS_EMPTY' => 'Please, repeat your password',
    'EMAIL_EMPTY' => 'Please, enter your email',
    'BITRHDAY_EMPTY' => 'Please, enter your brithdate',
    'BITRHDAY_INCORRECT' => 'Please enter your correct date of birth',
    'BITRHDAY_NOT18' => 'Registration is available to persons under 18 years of',
    'SEX_EMPTY' => 'Please select your gender',

    //месяца
    'MONTH' => 'Month',
    'DAY' => 'Day',
    'YEAR' => 'Year',
    'JAN' => 'January',
    'FEB' => 'February',
    'MART' => 'March',
    'APRIL' => 'April',
    'MAI' => 'May',
    'JUNE' => 'June',
    'JULE' => 'July',
    'AUGUST' => 'August',
    'SEPT' => 'September',
    'OCT' => 'October',
    'NOV' => 'November',
    'DEC' => 'December',

];