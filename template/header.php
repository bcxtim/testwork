<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.12.15
 * Time: 12:58
 */


require_once($_SERVER['DOCUMENT_ROOT']."/classes/Translator.php");



//проверяем, пустая ли сессия с языком, и если пустая, то возвращаем по дефолту русский язык
if(empty($_SESSION['lang']))
{
    $_SESSION['lang'] = 'ru';
}

header("Content-type: text/html; charset=utf-8");

//установка языка
if(isset($_GET['lang']))
{

    if($_GET['lang'] == 'ru')
    {
        $_SESSION['lang'] = $_GET['lang'];

        header("Location: index.php");
    } elseif($_GET['lang'] == 'en')
    {
        $_SESSION['lang'] = $_GET['lang'];
        header("Location: index.php");
    }
}




?>
<script>
    var messages = <?php echo json_encode(Translator::getMessages()); ?>
</script>

<DOCTYPE html>
    <html>
        <head>
            <title><?= Translator::message('TITLE'); ?></title>
            <script src="../js/script.js"></script>
            <link rel="stylesheet" href="../css/style.css">

        </head>
        <body>
        <div class="wrapper">

            <header>
                <a href="?lang=ru">RU</a>
                <a href="?lang=en">EN</a>
                <nav>
                    <?php if(isset($_SESSION['id'])): ?>
                        <a href="../logout.php"><?= Translator::message('LINK_LOGOUT'); ?></a>
                    <?php else: ?>
                        <a href="../login.php"><?= Translator::message('LINK_LOGIN'); ?></a> <?= Translator::message('OR'); ?>
                        <a href="../registration.php"><?= Translator::message('LINK_REG'); ?></a>
                    <?php endif; ?>
                </nav>

            </header>
            <div class="content">






