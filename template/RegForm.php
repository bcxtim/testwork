<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.01.16
 * Time: 12:55
 */
require_once('header.php');
ini_set('display_errors',1);
error_reporting(E_ALL);
?>


<p><h3><?= Translator::message('REG_MESSAGE'); ?></h3></p>

<?= $errTitle; ?>
<?php foreach($err as $errMessage): ?>
<?= "<span class='error'>".$errMessage."</span><br />"; ?>
<?php endforeach; ?>
<form action="../registration.php"  onsubmit="return validate_regform(this)" method="post" enctype="multipart/form-data">
    <p>
        <label for="login"><?= Translator::message('USER_LOGIN'); ?></label>
        <input type="text" name="login" id="login" value="<?php if(isset($login)) echo $login; ?>"  />
        <span id="err_login" class="error"></span>
    </p>

    <p>
        <label for="first_name"><?= Translator::message('USER_FIRST_NAME'); ?></label>
        <input type="text" name="first_name" id="first_name" value="<?php if(isset($first_name)) echo $first_name; ?>" />
        <span id="err_firstname" class="error"></span>
    </p>

    <p>
        <label for="last_name"><?= Translator::message('USER_LAST_NAME'); ?></label>
        <input type="text" name="last_name" id="last_name"  value="<?php if(isset($last_name)) echo $last_name; ?>" />
        <span id="err_lastname" class="error"></span>
    </p>

    <p>
        <label for="password"><?= Translator::message('USER_PASSWORD'); ?></label>
        <input type="password" name="password"  id="password" />
        <span id="err_password" class="error"></p>
    </p>
    <p>
        <label for="repassword"><?= Translator::message('USER_REPASSWORD'); ?></label>
        <input type="password" name="repassword"  id="repassword" />
        <span id="err_repassword" class="error"></p>
    </p>

    <p>
        <label for="email"><?= Translator::message('USER_EMAIL'); ?></label>
        <input type="email" name="email" id="email" value="<?php if(isset($email)) echo $email; ?>" />
        <span id="err_email" class="error"></span>
    </p>
    <p>
        <label for="sex"><?= Translator::message('USER_SEX'); ?></label>
        <input type="radio" name="sex" value="male" id="male" /> <?= Translator::message('USER_SEX_MALE'); ?>
        <input type="radio" name="sex" id="female" value="female" /> <?= Translator::message('USER_SEX_FEMALE'); ?>
        <span id="err_sex" class="error"></span>
    </p>
    <p>
        <label><?= Translator::message('USER_BIRTH_DATE'); ?></label>

        <select name="day" id="day">
            <option value="0"><?= Translator::message('DAY'); ?></option>
            <?php
            $dayOptions = '';
            for($day = 1; $day <= 31; $day++)
            {
                $dayOptions .= "<option value='".$day."'>".$day."</option>";
            }
            echo $dayOptions;
            ?>
        </select>
        <select name="month" id="month">
            <option value="0" ><?= Translator::message('MONTH'); ?></option>
            <option value="1" ><?= Translator::message('JAN'); ?></option>
            <option value="2" ><?= Translator::message('FEB'); ?></option>
            <option value="3" ><?= Translator::message('MART'); ?></option>
            <option value="4" ><?= Translator::message('APRIL'); ?></option>
            <option value="5" ><?= Translator::message('MAI'); ?></option>
            <option value="6" ><?= Translator::message('JUNE'); ?></option>
            <option value="7" ><?= Translator::message('JULE'); ?></option>
            <option value="8" ><?= Translator::message('AUGUST'); ?></option>
            <option value="9" ><?= Translator::message('SEPT'); ?></option>
            <option value="10" ><?= Translator::message('OCT'); ?></option>
            <option value="11" ><?= Translator::message('NOV'); ?></option>
            <option value="12" ><?= Translator::message('DEC'); ?></option>

        </select>
        <select name="year" id="year">
            <option value="0"><?= Translator::message('YEAR'); ?></option>
            <?php
            $yearOptions = '';
            for($year = 1998; $year >= 1930; $year--)
            {
                $yearOptions .= "<option value='".$year."'>".$year."</option>";
            }
            echo $yearOptions;
            ?>
        </select>
        <span id="err_birthdate" class="error"></span>
    </p>
    <p>
        <label><?= Translator::message('USER_AVATAR'); ?></label>
        <input type="file" name="image" />
    </p>

    <input type="submit" class="btn" name="registration" onsubmit="return" value="<?= Translator::message('USER_SIGN_UP'); ?>" >
    <br /><br />

</form>


<?php
require_once('footer.php');