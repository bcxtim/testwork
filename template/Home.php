<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.01.16
 * Time: 13:58
 */

require_once('header.php');

?>
<?= $hello; ?>
<?php if(isset($user_id)): ?>
    <h3><?= Translator::message('USER_DATA'); ?></h3><br/>
    <table border="1">
        <tr>
            <th><?= Translator::message('USER_ID'); ?></th>
            <th><?= Translator::message('USER_LOGIN'); ?></th>
            <th><?= Translator::message('USER_FIRST_NAME'); ?></th>
            <th><?= Translator::message('USER_LAST_NAME'); ?></th>
            <th><?= Translator::message('USER_EMAIL'); ?></th>
            <th><?= Translator::message('USER_SEX'); ?></th>
            <th><?= Translator::message('USER_BIRTH_DATE'); ?></th>
            <th><?= Translator::message('USER_AVATAR'); ?></th>
            <th><?= Translator::message('USER_DATE_REG'); ?></th>
        </tr>
        <tr>
            <td><?= $user['id']; ?></td>
            <td><?= htmlspecialchars($user['login']); ?></td>
            <td><?= htmlspecialchars($user['first_name']); ?></td>
            <td><?= htmlspecialchars($user['last_name']); ?></td>
            <td><?= htmlspecialchars($user['email']); ?></td>
            <td><?= $user['sex']; ?></td>
            <td><?= $user['date_user']; ?></td>
            <td>
                <?php if(!empty($user['image'])): ?>
                    <img src="images/<?= $user['image']; ?>" width="150" height="150" alt="">
                <?php else: ?>
                    <?= Translator::message('NO_AVATAR'); ?>
                <?php endif; ?>

            </td>
            <td><?= $user['date_reg']; ?></td>
        </tr>
    </table>

<?php else:?>
    <h2><?= Translator::message('TEXT_INDEX'); ?></h2>
<?php endif; ?>


<?php
require_once('footer.php');
