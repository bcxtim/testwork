<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.01.16
 * Time: 11:14
 */
require_once('header.php');

?>

<form action="../login.php" method="post" onsubmit=" return validate_loginform(this)">
<?= $err; ?>
    <p>
        <label for="login"><?= Translator::message('USER_LOGIN'); ?></label>
        <input type="text" name="login" id="login">
        <span id="err_login" class="error"></span>
    </p>
    <p>
        <label for="password"><?= Translator::message('USER_PASSWORD'); ?></label>
        <input type="password" name="password" id="password">
        <span id="err_password" class="error"></span>
    </p>
    <input type="submit" class="btn" name="enter" value="<?= Translator::message('USER_ENTER'); ?>">
</form>

<?php
require_once('footer.php');