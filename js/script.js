/**
 * Created by root on 08.01.16.
 */


function validate_regform()
{
    var login = document.getElementById('login');
    var firstname = document.getElementById('first_name');
    var lastname = document.getElementById('last_name');
    var password = document.getElementById('password');
    var email = document.getElementById('email');
    var day = document.getElementById('day');
    var month = document.getElementById('month');
    var year = document.getElementById('year');
    var login_reg = new RegExp('^[a-zA-Z0-9]+$');
    var repass = document.getElementById('repassword');
    var female = document.getElementById('female');
    var male = document.getElementById('male');
    var birthDay = new Date(year.value, month.value, day.value).getTime();
    var checkdate = new Date(new Date().getYear()-18, new Date().getMonth()+1, new Date().getDate()).getTime();


    var login_err = document.getElementById('err_login');
    var pass_err = document.getElementById('err_password');
    var repass_err = document.getElementById('err_repassword');
    var firstname_err = document.getElementById('err_firstname');
    var lastname_err = document.getElementById('err_lastname');
    var email_err = document.getElementById('err_email');
    var birthdate_err = document.getElementById('err_birthdate');
    var sex_err = document.getElementById('err_sex');

    var valid = true;

    //проверка логина
    if(login.value == '') {
        login_err.innerHTML = messages.LOGIN_EMPTY; valid = false;
    } else if(login.value.length < 3 || login.value.length > 10 ) {
        login_err.innerHTML = messages.LOGIN_LENGTH; valid = false;
    } else if(!login_reg.test(login.value)) {
        login_err.innerHTML = messages.LOGIN_LAT; valid = false;
    } else {
        login_err.innerHTML = '';
    }

    //проверка имени
    if(firstname.value == '') {
        firstname_err.innerHTML = messages.FIRSTNAME_EMPTY; valid = false;
    } else {
        firstname_err.innerHTML = '';
    }

    //проферка фамилии
    if(lastname.value == '') {
        lastname_err.innerHTML = messages.LASTNAME_EMPTY; valid = false;
    } else {
        lastname_err.innerHTML = '';
    }

    //проверка пароля
    if(password.value == '') {
        pass_err.innerHTML = messages.PASS_EMPTY; valid = false;
    } else if(password.value.length < 6 || password.value.length > 12) {
        pass_err.innerHTML = messages.PASSWORD_LENGTH; valid = false;
    } else
    {
        pass_err.innerHTML = '';
    }
    //проверка пароля
    if(repass.value == '') {
        repass_err.innerHTML = messages.REPASS_EMPTY; valid = false;
    } else if(repass.value != password.value) {
        repass_err.innerHTML = messages.REPASS_INCORRECT; valid = false;
    } else
    {
        repass_err.innerHTML = '';
    }

    //проверка е-маила
    if(email.value == '') {
        email_err.innerHTML = messages.EMAIL_EMPTY; valid = false;
    } else {
        email_err.innerHTML = '';
    }

    //проверка пола
    if(male.checked == false && female.checked == false) {
        sex_err.innerHTML = messages.SEX_EMPTY; valid = false;
    } else {
        sex_err.innerHTML = '';
    }

    //проверка даты рождения
    if(day.value == 0 || month.value == 0 || year.value == 0) {
        birthdate_err.innerHTML = messages.BITRHDAY_EMPTY; valid = false;
    } else if(day.value < 0 || day.value > 31 || month.value < 0 || month.value > 12 || year.value < 0 || year.value > 1998 ) {
        birthdate_err.innerHTML = messages.BITRHDAY_INCORRECT; valid = false;
    } else if(birthDay > checkdate) {
        birthdate_err.innerHTML = messages.BITRHDAY_NOT18; valid = false;
    } else {
        birthdate_err.innerHTML = '';
    }




    return valid;



}

function validate_loginform(){
    var valid = true;
    var login = document.getElementById('login');
    var password = document.getElementById('password');

    var err_login = document.getElementById('err_login');
    var err_password = document.getElementById('err_password');

    if(login.value == '') {
        err_login.innerHTML = messages.LOGIN_EMPTY; valid = false;
    } else {
        err_login.innerHTML = '';
    }
    if(password.value == '') {
        err_password.innerHTML = messages.PASS_EMPTY; valid = false;
    } else {
        err_password.innerHTML = '';
    }

    return valid;
}