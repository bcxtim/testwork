<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/db/ActiveRecord.php');
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.01.16
 * Time: 13:04
 */

class UserModel extends ActiveRecord
{
    public $table = 'users';


    /**
     * Создание нового юзера
     * @param $login
     * @param $first_name
     * @param $last_name
     * @param $password
     * @param $email
     * @param $sex
     * @param $birth_date
     * @param $image_name
     * @return bool|mysqli_result
     */
    public function createUser($login, $first_name, $last_name, $password, $email, $sex, $birth_date, $image_name)
    {
        $result = $this->create([
            'login' => $login,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'password' => $password,
            'email' => $email,
            'sex' => $sex,
            'date_user' => $birth_date,
            'image' => $image_name
        ]);
        $this->uploadFile($image_name);
        $user = $this->fetchArrayOne($result);

        return $user;

    }

    /**
     * Загрузка аватара
     * @param $image_name
     * @return bool
     */
    public function uploadFile($image_name)
    {
        $image = move_uploaded_file($_FILES['image']['tmp_name'], "images/".$image_name);
        return $image;
    }



    /**
     * Авторизоваться на сайте
     * @param $login
     * @param $password
     * @return array|null
     */
    public function loginUser($login, $password)
    {
        $result = $this->selectAllByParam('*',
            [
            'login' => $login,
            'password' => $password
            ]
        );

        $user_data = $this->fetchArrayOne($result);
        return $user_data;
    }

    /**
     * Проверка, стоит ли сессия. Нужно для проверки авторизации
     * @return bool
     */
    public function isUserLoggedIn()
    {
        if(isset($_SESSION['id']))
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * Вытащить айди последнего созданного юзера
     * @return int|string
     */
    public function getLastIdUser()
    {
        $user_id = $this->getLastId();
        return $user_id;
    }

    /**
     * Отобразить все данные юзера по id
     * @param $id
     * @return array|null
     */
    public function selectAllByUserId($id)
    {
        $user_id = (int)$id;
        $user_data = $this->selectAllById('*', $user_id);

        $user = $this->fetchArrayOne($user_data);
        return $user;
    }


}