<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/model/UserModel.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/Translator.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/classes/FormValidator.php');
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.01.16
 * Time: 9:49
 */
session_start();

class UserController
{
    public $model;

    /**
     * Подключение UserModel
     */
    public function __construct()
    {
        $model = new UserModel();
        $this->model = $model;
    }

    /**
     * Авторизация
     * @return string
     */
    public function actionLogin()
    {

        //если пользователь авторизован, не открывать форму авторизации
        if($this->model->isUserLoggedIn())
        {
            header("Location: index.php");
        }

        $err = '';

        if(isset($_POST['enter']))
        {

            $login = $_POST['login'];
            $password = md5($_POST['password']);
            $user = $this->model->loginUser($login, $password);
            if($user['login'] == $login && $user['password'] == $password)
            {
                $_SESSION['id'] = $user['id'];
                $_SESSION['first_name'] = $user['first_name'];
                header("Location: index.php");
            } else
            {
                $err =  "<span class='error'>".Translator::message('AUTH_INCORRECT')."</span>";

            }

        }
        include($_SERVER['DOCUMENT_ROOT'].'/template/LoginForm.php');
        return $err;

    }

    /**
     * Регистрацция пользователя
     * @return string
     */
    public function actionReg()
    {
        $form_validator = new FormValidator();
        $err = array();
        $errTitle = '';

        //если пользователь авторизован не открывать форму регистрации
        if ($this->model->isUserLoggedIn()) {
            header("Location:index.php");
        }


        if (isset($_POST['registration'])) {
            $login = $_POST['login'];
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $password = md5($_POST['password']);
            $repassword = md5($_POST['repassword']);
            $email = $_POST['email'];
            $day = $_POST['day'];
            $month = $_POST['month'];
            $year = $_POST['year'];
            $birth_day = date("$year-$month-$day");
            $image_name = $_FILES['image']['name'];
            $typeFile = $_FILES['image']['type'];
            $fileSize = $_FILES['image']['size'];


            $countLogin = $form_validator->checkCountParamInDb('users', 'login', $login); // количество одинаковых записей с логином
            $countEmail = $form_validator->checkCountParamInDb('users', 'email', $email); // количество одинаковых записей с е-маилом
            $loginPregMatch = $form_validator->pregMatch('/^[a-zA-Z0-9]+$/', $login); // проверка на совпадение рег. выражению.
            $emailPregMatch = $form_validator->pregMatch('/^[a-zA-Z0-9]+@[a-zA-Z0-9]+[.a-zA-Z0-9]+$/', $email);

            //проверяем логин
            if (strlen($login) < 3 or strlen($login) > 10) {
                $err[] = Translator::message('LOGIN_LENGTH');
            } else if ($loginPregMatch == true) {
                $err[] = Translator::message('LOGIN_LAT');
            } else if ($countLogin > 0) {
                $err[] = Translator::message('LOGIN_USE');
            }


            // проверяем длину пароля
            if (strlen($_POST['password']) < 6 or strlen($_POST['password']) > 12) {
                $err[] = Translator::message('PASSWORD_LENGTH');
            }

            //проверяем повторный пароль
            if ($repassword != $password) {
                $err[] = Translator::message('REPASS_INCORRECT');
            }


            // проверяем имя и фамилию
            if ($first_name == '') {
                $err[] = Translator::message('FIRSTNAME_EMPTY');
            }

            if ($last_name == '') {
                $err[] = Translator::message('LASTNAME_EMPTY');
            }

            //проверяем корректность е-маила
            if ($email == '') {
                $err[] = Translator::message('EMAIL_EMPTY');
            } else if ($emailPregMatch == true) {
                $err[] = Translator::message('INCORECT_EMAIL');
            } else if ($countEmail > 0) {
                $err[] = Translator::message('EMAIL_USE');
            }

            //проверка, выбран ли пол
            if (isset($_POST['sex']) == null) {
                $err[] = Translator::message('SEX_EMPTY');
            }

            //проверяем, если выбрана дата рождения
            if ($day == 0 || $month == 0 || $year == 0) {
                $err[] = Translator::message('BITRHDAY_EMPTY');
            } else if ($day > 31 || $day < 0 || $month < 0 || $month > 12 || $year < 0 || $year > 1998) {
                $err[] = Translator::message('BITRHDAY_INCORRECT');
            } else if (strtotime($year . "-" . $month . "-" . $day) >= strtotime("-18 year")) {
                $err[] = Translator::message('BITRHDAY_NOT18');
            }

            //проверка, на загрузку файла
            if ($_FILES['image']['error'] == 0) {
                // если файл загружается, проверяем на формат
                if ($typeFile != 'image/jpeg' && $typeFile != 'image/gif' && $typeFile !== 'image/png') {
                    $err[] = Translator::message('FILE_TYPE');
                }

                // если файл загружается, то проверяем на размер файла
                if ($fileSize > 1048576) {
                    $err[] = Translator::message('FILE_SIZE');
                }
            }

            //если ошибок нет, то регистрируем пользователя. Если есть, то выводим ошибки.
            if (count($err) == 0) {

                $user = $this->model->createUser($login, $first_name, $last_name, $password, $email, $_POST['sex'], $birth_day, $image_name);
                $_SESSION['id'] = $user['id'];
                $_SESSION['first_name'] = $user['first_name'];

                header("Location: index.php");
            } else {
                $errTitle = "<span class='error'><strong>".Translator::message('ERROR_MESSAGE')."</strong></span><br /> <br />";

            }

        }
        include($_SERVER['DOCUMENT_ROOT'].'/template/RegForm.php');
        return $errTitle;
    }

    /**
     * Отображение главной страницы и данных пользователя, если пользователь авторизован
     * @return string
     */
    public function actionIndex()
    {
        $user_login = $this->model->isUserLoggedIn();

        //Если пользователь авторизован , и есть сессия, то выводим данные юзера по id,  где id юзера равно id сессии
        if(isset($_SESSION['id']))
        {
            $user_id = $_SESSION['id'];
            $user = $this->model->selectAllByUserId($user_id);
        }

        //если пользователь авторизован, выводим привествие
        if($user_login)
        {
            $hello =  "<h2>".Translator::message('HELLO').", ".htmlspecialchars($user['first_name'])."</h2><br/><br/>";
        }
        include($_SERVER['DOCUMENT_ROOT']."/template/Home.php");
        return $hello;
    }
}