<?php
require_once($_SERVER['DOCUMENT_ROOT']."/classes/db/ActiveRecord.php");
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08.01.16
 * Time: 13:24
 */
class FormValidator extends ActiveRecord
{
    /** Вернуть количество одинаковых значений в столбце с парамтром
     * @param table
     * @param $param
     * @param $column
     * @return mixed
     */
    public function checkCountParamInDb($table, $column, $param)
    {
        $user_param = $this->checkParamInDb($table, $column, $param);
        $paramCountArray = $this->fetchArrayOne($user_param);
        $paramCount = reset($paramCountArray);
        return $paramCount;
    }

    /**
     * Проверка на регулярные выражения
     * @param $attr
     * @param $field
     * @return bool
     */
    public function pregMatch($attr, $field)
    {
        $value = !preg_match($attr, $field);
        return $value;
    }


}