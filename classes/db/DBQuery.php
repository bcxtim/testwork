<?php
require_once('Db.php');

/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.01.16
 * Time: 18:00
 */
class DBQuery extends Db
{


    /**
     * Выбрать все из таблицы
     * @param $columns
     * @param $table
     * @return PDOStatement
     */
    public function selectAllinDb($columns, $table)
    {
        if(is_array($columns))
        {
            $columns = implode(', ', $columns);
        }
        $query = "SELECT $columns FROM $table";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Выбор всех результатов по запросу и загоняем в ассоциативный массив
     * @param $result
     * @return array
     */
    public function fetchArrayAll($result)
    {
        $rows = array();
        while($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $rows[$row['id']] = $row;
        }
        return $rows;
    }

    /**
     * Выбор все из БД по id
     * @param $columns
     * @param $table
     * @param $id
     * @return PDOStatement
     */
    public function selectAllByIdInDb($columns, $table, $id)
    {

        $query = $this->prepare("SELECT $columns FROM $table WHERE id = ?");
        $query->execute([$id]);

        return $query;
    }

    /**
     * Вывод одного результата в ассоциативном массиве
     * @param $result
     * @return mixed
     */

    public function  fetchArrayOne($result)
    {
        $row = $result->fetch(PDO::FETCH_ASSOC);

        return $row;
    }



    /**
     * Удалить из таблицы строку по ID
     * @param $table
     * @param $id
     * @return bool|mysqli_result
     */
    public function deleteByIdInDb($table, $id)
    {
        $query = "DELETE FROM $table WHERE id = $id";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Создать новую запись в таблице и вернуть айди
     * @param $table
     * @param $params
     * @return $id
     */
    public function createInDb($table, $params)
    {
        $setParts = [];
        $values = [];
        $attr = [];

        foreach($params as $columns => $value)
        {
            $setParts[] = $columns;
            $values[] = $value;
            $attr[] = "?";
        }
        $query = $this->prepare("INSERT INTO $table (" . implode(', ', $setParts) . ") VALUES (" . implode(', ', $attr) . ")");
        $id = null;
        if($query->execute($values))
        {
            $id = $this->getLink()->lastInsertId();

        }
       if(is_null($id))
       {
           exit("Error. Try again");
       }

        return $id;


    }

    /**
     * Обновить в таблице по ID
     * @param $table
     * @param $params
     * @param $id
     * @return bool|mysqli_result
     */

    public function updateInDb($table, $params, $id)
    {
        $setParts = [];

        foreach($params as $columns => $value)
        {
            $setParts[] = $columns . ' = "'.$value.'"';

        }
        $query = "UPDATE $table SET ". implode(', ', $setParts)." WHERE id = $id";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Проверка, на количество совпадений айди, с одинаковым параметром в колонке
     * @param $table
     * @param $column
     * @param $param
     * @return int
     */
    public function checkParamInDb($table, $column, $param)
    {
        $count = $this->prepare("SELECT COUNT(id) FROM $table WHERE $column = :param");
        $count->bindParam(':param', $param);
        $count->execute();
        return $count;
    }

    /** Выбрать нужные данные , по определенным параметрам
     * @param $table
     * @param column
     * @param $param
     * @return mixed
     */
    public function selectAllByParamInDb($table, $column, $param)
    {
        $setParts = [];
        $values = [];
        foreach($param as $columns => $value)
        {
            $setParts[] = $columns . ' = ?';
            $values[] = $value;

        }
        $query = $this->prepare("SELECT $column FROM $table WHERE ". implode(' AND ', $setParts));
        $query->execute($values);

        return $query;
    }

}