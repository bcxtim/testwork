<?php

class Db {


    //параметры подключения к БД
    private $host = 'localhost';
    private $username = 'bcxtim';
    private $password = '13123974';
    private $database = 'test';
    private $link = null;

    /**
     * Подключение к БД
     */
    public function __construct()
    {
        $host = $this->host;
        $username = $this->username;
        $password = $this->password;
        $database = $this->database;
        $dsn = "mysql:host=$host;dbname=$database";


        $this->link = new PDO($dsn, $username, $password);
        $this->link->exec("SET NAMES utf8");



    }

    /**
     * Возвращение соединения к БД
     * @return mysqli|null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Возвращает строку
     * @param $query
     * @return PDOStatement
     */
    public function query($query)
    {
        return $this->link->query($query);

    }

    /**
     * @param $query
     * @return PDOStatement
     */
    public function prepare($query)
    {
        return $this->link->prepare($query);

    }






}