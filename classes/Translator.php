<?php


/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.01.16
 * Time: 20:33
 */
class Translator
{
    public static $messages;


    /**
     * Вывод сообщений по имени
     * @param $name
     * @return string
     */
    public static function message($name)
    {
        self::loadMessages();
        return isset(self::$messages[$name]) ? self::$messages[$name] : '';
    }

    /**
     * Загрузчик сообщений
     */
    private static function loadMessages()
    {
        if(is_null(self::$messages))
        {
            $lang = $_SESSION['lang'];
            self::$messages = require($_SERVER['DOCUMENT_ROOT'].'/messages/'.$lang.'.php');
        }
    }

    /**
     * Получение всех сообщений в массив
     * @return mixed
     */
    public static function getMessages()
    {
        self::loadMessages();
        return self::$messages;
    }
}