<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.01.16
 * Time: 17:00
 */
session_start();

unset($_SESSION['id']);
unset($_SESSION['first_name']);

header('Location: index.php');